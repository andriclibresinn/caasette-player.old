import '@babel/polyfill'

import React, { useContext, useReducer } from 'react';
import ReactDOM from 'react-dom';

import Store from './todo-app/context';
import reducer from './todo-app/reducer';
import TodoList from './todo-app/components/TodoList';
import TodoForm from './todo-app/components/TodoForm';
import RemoteComponent from './caasette-loadable'
import { usePersistedContext, usePersistedReducer } from './todo-app/usePersist';

const App = () => {
  // create a global store to store the state
  const globalStore = usePersistedContext(useContext(Store), 'state');

  // `todos` will be a state manager to manage state.
  const [state, dispatch] = usePersistedReducer(
    useReducer(reducer, globalStore),
    'state' // The localStorage key
  );

  return (
    // State.Provider passes the state and dispatcher to the down
    <Store.Provider value={{ state, dispatch }}>
      <TodoForm />
      <TodoList />
      <RemoteComponent name="@caasette/ReactRectanglePopupMenu/Simple" />
    </Store.Provider>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);

if (module.hot) {
  module.hot.accept('./todo-app/index', () => render(App));
}
