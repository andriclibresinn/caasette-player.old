// import waitForReact from '../scratch/WaitForReact'
import { stdOnEnd, ieOnEnd } from './utils'

const SORRY_IM_USING_WINDOW = window
const noop = () => {}
const caasettes = {}
const scripts = {}
const PARTS = {
  RESOLVER: 0,
  CASE: 1,
  CAASETTE: 2,
}

export const scriptNode = (src) => {
  const script = document.createElement('script')
  script.type = 'text/javascript'
  script.charset = 'utf8'
  script.async = 'async'
  script.src = src
  script.setAttribute('caasette', `@repo-manager/components/${src}`)
  script.setAttribute('recycle', false)

  return script
}

export const append = (src, cb = noop) => {
  const head = document.head || document.getElementsByTagName('head')[0]
  const script = scriptNode(src)
  const finishedHandler = !script.onload ? stdOnEnd : ieOnEnd

  finishedHandler(script, cb)

  scripts[src] = script
  head.appendChild(script)
}

// @TODO: convert to async await.
export const load = (componentName) => (
  new Promise((resolve, reject) => {
    const parts = componentName.split('/')
    const caasette = parts[PARTS.CAASETTE]
    const cartridge = parts[PARTS.CASE]

    // Debug until we set something up for resolvers.
    // const url = `http://localhost:3000/${cartridge}.js`
    const url = `http://localhost:3001/@caasettes/${cartridge}.js`

    // Check the cache.
    if (caasettes[url]) {
      resolve(caasettes[url])
      return
    }

    // Load the script.
    append(url, (err) => {
      if (err) {
        return reject(err)
      }

      // @TODO: This is saying that it was loaded. Don't think I need.
      // Besides we want to use a different namespace, specifically:
      // window.caasetteplayer['@caasette/foo/bar']
      if (SORRY_IM_USING_WINDOW[cartridge][caasette]) {
        caasettes[url] = SORRY_IM_USING_WINDOW[cartridge][caasette]
        resolve(caasettes[url])
      }
      else {
        reject(new Error(`Could not load ${componentName} from ${url}`))
      }
    })
  })
)

// @TODO: This should not be required in the future.
export default (componentName) => (
  waitForReact.then(() => load(componentName))
)
