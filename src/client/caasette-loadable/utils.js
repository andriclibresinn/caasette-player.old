 // eslint-disable-next-line
const caasettes = {}

// Get all the components.
export const components = () => {}
export const player = () => {}

// Return a new script DOM element.
export const script = (id) => {
  const script = document.createElement('script')

  script.type = 'text/javascript'
  script.charset = 'utf8'
  script.src = `http://localhost:3000/${id}.js`
  script.setAttribute('caasette', id);
  // script.async = 'async' in opts ? !!opts.async : true

  return script
}

// Create a script DOM Element

export const setAttributes = (script, attrs) => {
  for (var attr in attrs) {
    script.setAttribute(attr, attrs[attr]);
  }
}

export const stdOnEnd = (script, cb) => {
  script.onload = function () {
    this.onerror = this.onload = null
    cb(null, script)
  }
  script.onerror = function () {
    // this.onload = null here is necessary
    // because even IE9 works not like others
    this.onerror = this.onload = null
    cb(new Error('Failed to load ' + this.src), script)
  }
}

export const ieOnEnd = (script, cb) => {
  script.onreadystatechange = function () {
    if (this.readyState != 'complete' && this.readyState != 'loaded') return
    this.onreadystatechange = null
    cb(null, script) // there is no way to catch loading errors in IE8
  }
}

// // Function to get the options attribute. This is specific for the load-script package.
// export const scriptOptions = (name) => ({
//   attrs: {
//     caasette: `@repo-manager/components/${name}`
//   }
// })
