import React from 'react'

// Take a closer look at: https://blog.logrocket.com/conditional-rendering-in-react-c6b0e5af381e
export default (Component) => {
  return function EnhancedComponent(props) {
    if (condition) {
      return <AnotherComponent {...props} />
    }

    return <Component {...props} />
  };
}
