// This will be the main thingy later.
import React, { useEffect, useState } from 'react'
import { load as LoadCaasette} from './loadCaasette'

const useFetch = caasette => {
  const [data, setData] = useState(null)
  const [loading, setLoading] = useState(true)

  // Similar to componentDidMount and componentDidUpdate:
  useEffect(async () => {
    const caasette = await LoadCaasette(caasette)
    console.log('caasette', caasette)
    const data = await response.json()
    const [item] = data.results
    setData(item)
    setLoading(false)
  }, [])

  return { data, loading }
}

export default ({ name }) => {
  const [count, setCount] = useState(0)
  const { data, loading } = useFetch(name)

  console.log('data', data)

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
      {loading ? <div>...loading</div> : <div>{data.name.first}</div>}
    </div>
  )
}

// https://github.com/slorber/react-async-hook

// @TODO: Use React Hooks: https://reactjs.org/docs/hooks-intro.html
// class RemoteComponent extends React.Component {
//   async componentDidMount() {    
//     this.Component = await LoadCaasette(this.props.name)
//     this.forceUpdate()
//   }

//   // We want this to be a proper HoC that "branches" based on props.
//   // Do this via React Hooks.
//   render = () => {
//     return typeof this.Component === 'function'
//       ? <this.Component />
//       : <div>Loading</div>
//   }
// }

// export default RemoteComponent