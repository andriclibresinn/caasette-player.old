import { createContext } from 'react'

// Store Context is the global context that is managed by reducers.
export default createContext({
  todos: [
    // Initial Data
    'Buy milk',
    'Some eggs',
    'Go to work'
  ]
})

// export default Store
