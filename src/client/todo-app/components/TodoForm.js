import React, { useContext, useState } from 'react'
import Store from '../context'

export default () => {
  const { dispatch } = useContext(Store)

  // Creating a local state to have currently writing
  // todo item that will be sent to the global store.
  const [todo, setTodo] = useState('')

  const handleTodoChange = (e) => {
    setTodo('asd')

    console.log(e.target.value)
  }

  // const handleTodoAdd = () => {
  //   dispatch({type: 'ADD_TODO', payload: todo })
  //   setTodo('')
  // }

  // const handleSubmitForm = (event) => {
  //   if (event.keyCode === 13) {
  //     handleTodoAdd()
  //   }
  // }

  return (
    <div className="row">
      <div className="col-md-12">
        <br />
        <div className="input-group">
          <input
            className="form-control"
            value={todo}
            autoFocus={true}
            placeholder="Enter new todo"
            // onKeyUp={handleSubmitForm}
            onChange={handleTodoChange}
          />
          <div className="input-group-append">
            <button className="btn btn-primary" onClick={() => {}}>
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
