import React, { useContext } from "react";
import Store from "../context";
import { TodoHeader } from "./TodoHeader";

const pluralize = count =>
  count > 1 ? `There are ${count} todos.` : `There is ${count} todo.`;

const Header = ({ todoCount }) => (
  todoCount === 0 ? (
    <h4>Yay! All todos are done! Take a rest!</h4>
  ) : (
    <TodoHeader>
      <span className="float-right">{pluralize(todoCount)}</span>
    </TodoHeader>
  )
)

export default () => {
  const { state, dispatch } = useContext(Store);

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="row">
          <div className="col-md-12">
            <br />
            <Header todoCount={state.todos.length} />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <ul className="list-group">
              {state.todos.map(t => (
                <li key={t} className="list-group-item">
                  {t}
                  <button
                    className="float-right btn btn-danger btn-sm"
                    style={{ marginLeft: 10 }}
                    // onClick={() => dispatch({ type: "COMPLETE", payload: t })}
                    onClick={() => console.log('hey')}
                  >
                    Complete
                  </button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
