import React from "react";

export const TodoHeader = (props) => (
  <div className="row">
    <div className="col-md-8">
      <h4>Todo List</h4>
    </div>
    <div className="col-md-4">
      {props.children}
    </div>
  </div>
)

// const header =
//   state.todos.length === 0 ? (
//     <h4>Yay! All todos are done! Take a rest!</h4>
//   ) : (
//     <TodoHeader>
//       <span className="float-right">{pluralize(state.todos.length)}</span>
//     </TodoHeader>
//   );